<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\LoggerController;
use App\Http\Controllers\SearchServerController;
use AWS;

class StoreToS3 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'store_to_s3';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check if there are any documents that have a pending S3 download url, if it is the case then download the document to s3 & update the download url in Elasticsearch';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // get all the documents in ES that have a pending download url
        $documents_info = array();
        $search_query = '{"query":{"term":{"ahu_download_url":"pending"} } }'; // get all the download_pending urls
        $sc = new SearchServerController();
        $result = $sc->search_es($search_query);

        if ($result != false) {
            $result = $sc->return_hits($result);
            if ($result['total']>0) {
                foreach ($result['hits'] as $k => $v) {
                    $documents_info[$k]['id'] = $v['_id'];
                    $documents_info[$k]['name'] = $v['_source']['name'];
                    $documents_info[$k]['category'] = $v['_source']['category'];
                    $documents_info[$k]['author'] = $v['_source']['author'];
                    $documents_info[$k]['description'] = $v['_source']['description'];
                    $documents_info[$k]['src_download_url'] = $v['_source']['src_download_url'];
                    $documents_info[$k]['document_url'] = $v['_source']['document_url'];
                }
            }
        }

        $downloads_directory = public_path().DIRECTORY_SEPARATOR."downloads".DIRECTORY_SEPARATOR;
        if (!empty($documents_info)) {
            foreach ($documents_info as $k => $v) {
                // download the document to S3
                $file_url = $documents_info[$k]['src_download_url'];
                $escaped_file_url = $this->escape_url($file_url);

                $doc_id = $documents_info[$k]['id'];
                $hash = substr($doc_id, 14);

                $parts = explode("/", $file_url);
                $c = count($parts);
                $c--;
                $file_name = $hash."_".$parts[$c];
                if (strlen($file_name)>256) { // unix support up to 256 => trim file name
                    $ext = explode(".", $file_name);
                    if (isset($ext[1])) {
                        $ext = $ext[1];
                        $file_name = substr($file_name, 0,250).".".$ext;
                    }else{
                        $ext = "";
                        $file_name = substr($file_name, 0,250);
                    }
                    
                }
                //download file to server

                if ($this->is_200($escaped_file_url)) {
                    file_put_contents($downloads_directory.$file_name, fopen($escaped_file_url, 'r'));
                    try {
                        // upload to s3
                        $s3 = AWS::createClient('s3');
                        $s3->putObject(array(
                            'Bucket'     => 'ahu-elibrary-resources',
                            'Key'        => $file_name,
                            'SourceFile' => $downloads_directory.$file_name,
                        ));
                    } catch (Exception $e) {
                        unlink($downloads_directory.$file_name);
                        $file = __FILE__; // get the current file's path
                        $line = __LINE__-2; // get the current file's line
                        $log  = new LoggerController(); // create a new instance of the logger
                        $log->Log('s3','alert',$e->getMessage(),$file,$line); // log to the log file
                        exit;
                    }

                    $documents_info[$k]['ahu_download_url'] = "https://s3.amazonaws.com/ahu-elibrary-resources/".$file_name;
                    unlink($downloads_directory.$file_name);
                }else{
                    $documents_info[$k]['ahu_download_url'] = "Error";

                    $file = __FILE__; // get the current file's path
                    $line = __LINE__-2; // get the current file's line
                    $log  = new LoggerController(); // create a new instance of the logger
                    $log->Log('document','error',"The current link has a problem '".$file_url."'",$file,$line); // log to the log file
                }
            }

            // update the urls in ES
            $update_query = "";
            if (!empty($documents_info)) {
                foreach ($documents_info as $k => $v) {
                    // build the bulk update query
                    $update_query .= '{ "update": { "_id": "'.$v['id'].'", "_retry_on_conflict" : 1} }'."\n";
                    $update_query .= '{ "doc" : {"name" : "'.$v['name'].'","category" : "'.$v['category'].'","author" : "'.$v['author'].'","description" : "'.$v['description'].'","src_download_url" : "'.$v['src_download_url'].'","document_url" : "'.$v['document_url'].'","ahu_download_url" : "'.$v['ahu_download_url'].'"} }'."\n";
                }
            }

            // update
            if (!empty($update_query)) {
                $sc = new SearchServerController();
                $sc->es_bulk($update_query);
            }
        }
    }

    // escape url for the fopen 
    public function escape_url($url)
    {
        // Check that the input data is sane
        if (!$parts = parse_url($url)) {
            return false;
        }
        if (!isset($parts['scheme'], $parts['host'])) {
            return false;
        }

        // construct site base URL
        $result = $parts['scheme'] . '://';

        if (isset($parts['user'])) {
            $result .= $parts['user'];
            if (isset($parts['pass'])) {
                $result .= ':' . $parts['pass'];
            }
            $result .= '@';
        }

        $result .= $parts['host'];

        // Normalize path
        if (!isset($parts['path'])) {
            // if no path assume domain root
            $parts['path'] = '/';
        }
        $parts['path'] = preg_split('#/+#', $parts['path']); // split to path components
        $parts['path'] = array_map(function($part) { // ensure all components are correctly escaped
            return urlencode(urldecode($part));
        }, $parts['path']);
        $parts['path'] = implode('/', $parts['path']); // reconstruct string
        $result .= $parts['path'];

        // parse the query string an rebuild it
        if (isset($parts['query'])) {
            parse_str($parts['query'], $query);
            if (function_exists('get_magic_quotes_gpc') && get_magic_quotes_gpc()) {
                // undo magic_quotes
                array_walk_recursive($query, function(&$value) {
                    $value = preg_replace('#\\\\([\'"\\\\\\x00])#', '$1', $value);
                });
            }
            $result .= '?' . http_build_query($query);
        }

        // add document fragment if present
        if (isset($parts['fragment'])) {
            $result .= '#' . $parts['fragment'];
        }

        return $result;
    }

    // return tru if the url was found
    public function is_200($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_exec($ch);
        // $is404 = curl_getinfo($ch, CURLINFO_HTTP_CODE) == 404;
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($code == 200) {
            return true;
        }else{
            return false;
        }
    }

}
