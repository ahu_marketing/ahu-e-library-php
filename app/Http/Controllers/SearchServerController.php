<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Config;
use Auth;

class SearchServerController extends Controller {

	public function search($query,$page,$sort = false,$sort_dir = false,$filters = false){
		$result = false;
		// build the search query
		$es_query = $this->return_search_query($query,$page,$sort,$sort_dir,$filters);
		$result = $this->search_es($es_query);

		if ($result != false) {
			$result = $this->return_hits($result);
			// return response($resp, 200)->header('Content-Type', "application/json");
			return $result;
		}else{
			// error
			return "error";
		}
	}

	function return_hits($json_object=''){
		$json_object = json_encode($json_object);
		$arr= json_decode($json_object, true);
		$hits = array();
		$total = 0;
		if (isset($arr['hits']['hits'])) {
			$hits = $arr['hits']['hits'];
		}

		if (isset($arr['hits']['total'])) {
			$total = $arr['hits']['total'];
		}

		return array("hits"=>$hits,"total"=>$total);
	}

	// return the search query to pass to elastic search
	public function return_search_query($query,$page = 1 ,$sort,$sort_dir,$filters ){

		$filter = "";
		if ($filters != false) {
			$filter = $this->build_filter($filters);
		}

		return $this->build_search_query($query,$filter,$page,$sort,$sort_dir);
	}


	// return the filter query
	function build_filter($filters){
		// $filters = json_decode($filters,true);
		// check if there are any filter available
		$category  		= $filters['c'];
		$document_title = $filters['dt'];
		$author_name    = $filters['a'];

		if ($category == "a") { // all categories
			$category = false;
		}

		$filter = "";
		$i = 0;

		if ($category != false) {
			switch ($category) {
				case 'cat1':
					$category = "val1";
					break;
			}
			$filter.='{"term":{"category" : "'.$category.'"}}';
			$i++;
		}

		$brackets = false;
		if (!empty($document_title) & !empty($author_name)) {
			$brackets = true;
		}


		if (!empty($document_title)) {
			if ($i>0) {
				$filter.= ",";
			}
			if ($brackets) {
				$filter.= "[";
			}
			$filter.='{"query": {"match": { "name": '.json_encode($document_title).' } } }';
			$i++;
		}

		if (!empty($author_name)) {
			if ($i>0) {
				$filter.= ",";
			}
			$filter.='{ "query": {"match": { "author": '.json_encode($author_name).' } } }';
			$i++;
		}

		if ($brackets) {
			$filter.= "]";
		}





		/*$filter.='{"not":{"terms":{"ahu_dwonload_url":["Error","pending"]}}}'; // dont show results that have Error/pending in download link
		$i++;*/

		/*switch ($type) {
			case 'post':
			case 'plan':
			case 'planned':
			case 'posted':
				$post_type = $data_type["post"];
				if (!empty($post_type)) {
					if ($i>0) {
						$filter.= ",";
					}
					$filter.='{"term":{"post_type" : "'.$post_type.'"}}';
					$i++;
				}

				$content_type = $data_type['content'];
				if (!empty($content_type)) {
					if ($i>0) {
						$filter.= ",";
					}
					$filter.='{"term":{"content_type" : "'.$content_type.'"}}';
					$i++;
				}
				
				if ($type == "plan") {
					if ($i>0) {
						$filter.= ",";
					}
					$filter.='{"term":{"is_draft" : "0"}}';
					$i++;	
				}

				if ($type == "planned" || $type == "posted") {
					$page = $data_type['page'];
					if (!empty($page) && $page != "0") {
						if ($i>0) {
							$filter.= ",";
						}
						$filter.='{"term":{"page" : "'.$page.'"}}';
						$i++;
					}

					$from = $data_type['from'];
					$to = $data_type['to'];
					if (!empty($from) || !empty($to)) {
						if ($i>0) {
							$filter.= ",";
						}
						$filter.='{"range":{"publish_on" : {';
						if (!empty($from)) {
							$filter.='"gte":"'.$from.'"';
						}

						if (!empty($to)) {
							if (!empty($from)) {
								$filter.=',';
							}
							$filter.='"lte":"'.$to.'"';
						}
						$filter.='}}}';
						$i++;
					}
				}
				break;
		}

		if (!empty($search_topic)) {
			if ($i>0) {
				$filter.= ",";
			}
			$search_topic = explode(",", $search_topic);
			$search_topic = '"'.implode('","', $search_topic).'"';
			$filter.='{"terms":{"topics" : ['.$search_topic.'],"execution":"and"}}';
			$i++;
		}

		if (!empty($search_season)) {
			if ($i>0) {
				$filter.= ",";
			}
			$search_season = explode(",", $search_season);
			$search_season = '"'.implode('","', $search_season).'"';
			$filter.='{"terms":{"seasons" : ['.$search_season.'],"execution":"and"}}';
			$i++;
		}

		if (!empty($search_emotion)) {
			if ($i>0) {
				$filter.= ",";
			}
			$search_emotion = explode(",", $search_emotion);
			$search_emotion = '"'.implode('","', $search_emotion).'"';
			$filter.='{"terms":{"emotions" : ['.$search_emotion.'],"execution":"and"}}';
			$i++;
		}

		if ($i>1) {
			$filter = "[".$filter."]";
		}*/

		return $filter;
	}


	function build_paginator_query($page)
	{
		$per_page = $this->per_page();
		$from = $page*$per_page-$per_page;
		if ($from<0) {
			$from = 0;
		}
		return '"from":'.$from.',"size":'.$per_page.',';
	}

	function build_search_query($searchQuery,$filter,$page,$sort,$sort_dir){
		$query = "";
		$query = '"query": {"multi_match":{"fields":["name.ar^1.3","name.en^1.3","author.analyzed^1.1","description.ar","description.en"], "query":'.json_encode($searchQuery).',"fuzziness": "AUTO","max_expansions":3,"prefix_length":2}}';
		$paginate = $this->build_paginator_query($page);

		$highlight = ',"highlight":{"pre_tags": ["<b>"],"post_tags": ["</b>"],"require_field_match":true,"fields":{"name.ar":{},"name.en":{},"author.analyzed":{},"description.ar":{},"description.en":{}}}';

		if (!empty($filter)) {
			$pre_query = '{'.$paginate.'"query":{"filtered" : {';
			$post_query = "";
			if (!empty($query)) {
				$post_query .= ',';
			}
			$post_query .= '"filter":{"bool":{"must":'.$filter/*.',"must_not":"'.*/.'"}} }}'.$highlight.'}';
		}else{
			$pre_query  = "{".$paginate;
			$post_query = $highlight."}";
		}

		$sort_query = "";
		if ($sort != false) {
			if (in_array($sort, array('re','dt','an'))) {
				switch ($sort) {
					case 're':
						$field = "_score";
						break;
					case 'dt': // document title
						$field = "name.name";
						break;
					case 'an': // author name
						$field = "author.author";
						break;
				}

				if (!in_array($sort_dir, array('asc','desc'))) {
					$sort_dir = "desc";
				}

				$sort_query = '"sort": { "'.$field.'": { "order": "'.$sort_dir.'" }},';
			}
		}

		$pre_query = '{'.$paginate.$sort_query.'"query":{"filtered" : {';
		$post_query = "";
		// $must_not = '"terms":{"ahu_download_url":["Error","pending"]}}';
		$must_not = '"terms":{"ahu_download_url":["Error"]}}';
		if (!empty($query)) {
			$post_query .= ',';
		}
		if (!empty($filter)) {
			$post_query .= '"filter":{"bool":{"must":'.$filter.',"must_not":{'.$must_not.'}} }}'.$highlight.'}';
		}else{
			$post_query .= '"filter":{"bool":{"must_not":{'.$must_not.'}} }}'.$highlight.'}';
		}

		return $pre_query.$query.$post_query;
	}

	protected function get_curl_instance()
	{
		if (!extension_loaded('curl')){
			$file = __FILE__; // get the current file's path
			$line = __LINE__-2; // get the current file's line
			$log  = new LoggerController(); // create a new instance of the logger
			$log->Log('curl','emergency',"Curl extention is DOWN !",$file,$line); // log to the log file
			return false;
		};

		$ch = curl_init();
		if ($ch) {
			self::set_curl_defaults($ch);
			return $ch;
		}else{
			$file = __FILE__; // get the current file's path
			$line = __LINE__-2; // get the current file's line
			$log  = new LoggerController(); // create a new instance of the logger
			$log->Log('curl','emergency',"A curl handler was not created",$file,$line); // log to the log file
			return false;
		}
	}

	protected function set_curl_option($ch, $option, $value = '')
	{
		curl_setopt($ch, $option, $value);
	}

	private function set_curl_defaults($ch)
	{
		// set it to the default value
		// (the number of seconds to wait while trying to connect)
		self::set_curl_option($ch, CURLOPT_CONNECTTIMEOUT_MS, 2000);

		//  set it to the default value
		// (the maximum number of seconds to allow cURL functions to execute)
		self::set_curl_option($ch, CURLOPT_TIMEOUT, 10);

		//  set it to the default value
		// (the maximum amount of persistent connections that are allowed)
		self::set_curl_option($ch, CURLOPT_MAXCONNECTS, 300);

		// set "CURLOPT_CLOSEPOLICY" to the default value
		// self::set_curl_option($ch, CURLOPT_CLOSEPOLICY, "CURLCLOSEPOLICY_OLDEST");

		// if "CURLOPT_RETURNTRANSFER" is always TRUE
		// (return the transfer as a string of instead of outputting it out directly)
		self::set_curl_option($ch, CURLOPT_RETURNTRANSFER, 1);

		self::set_curl_option($ch, CURLOPT_HEADER, 0);
	}

	// store n update
	public function store_in_es($id,$data){
		if (!isset($data)) {
			$file = __FILE__; // get the current file's path
			$line = __LINE__-2; // get the current file's line
			$log  = new LoggerController(); // create a new instance of the logger
			$log->Log('doc_create','emergency',"The post data to create a new doc was empty",$file,$line); // log to the log file
			return false;
		}

		$ch = self::get_curl_instance();
		$url = Config::get('alhuda.es_host')."/documents/doc/".$id;

		if (isset($url)) {
			self::set_curl_option($ch,CURLOPT_URL, $url);
			self::set_curl_option($ch,CURLOPT_PORT, 9200);
			self::set_curl_option($ch,CURLOPT_CUSTOMREQUEST, "POST");
			self::set_curl_option($ch,CURLOPT_POSTFIELDS, $data);

			$result = curl_exec($ch);
			// in case of error
			if (curl_errno($ch)) {
				$file = __FILE__; // get the current file's path
				$line = __LINE__-2; // get the current file's line
				$log  = new LoggerController(); // create a new instance of the logger
				$log->Log('curl','alert',"Curl error ".curl_error($ch),$file,$line); // log to the log file
				curl_close($ch);
				return false;
			}
			curl_close($ch);
			$result = json_decode($result);
			if (!isset($result->error)) { // if it was stored
				return true;
			}else{
				$file = __FILE__; // get the current file's path
				$line = __LINE__-2; // get the current file's line
				$log  = new LoggerController(); // create a new instance of the logger
				$log->Log('elasticsearch','error',"The doc was not properly inserted to elasticsearch",$file,$line); // log to the log file
				return false;
			}
		}else{
			$file = __FILE__; // get the current file's path
			$line = __LINE__-2; // get the current file's line
			$log  = new LoggerController(); // create a new instance of the logger
			$log->Log('elasticsearch','error',"The doc was not inserted to elasticsearch because the url was undefined",$file,$line); // log to the log file
			return false;
		}
	}

	public function search_es($es_query)
	{
		if (!isset($es_query)) {
			$file = __FILE__; // get the current file's path
			$line = __LINE__-2; // get the current file's line
			$log  = new LoggerController(); // create a new instance of the logger
			$log->Log('doc_create','emergency',"The post data to create a new doc was empty",$file,$line); // log to the log file
			return false;
		}

		$ch = self::get_curl_instance();
		$url = Config::get('alhuda.es_host')."/documents/doc/_search";

		if (isset($url)) {
			self::set_curl_option($ch,CURLOPT_URL, $url);
			self::set_curl_option($ch,CURLOPT_PORT, 9200);
			self::set_curl_option($ch,CURLOPT_CUSTOMREQUEST, "POST");
			self::set_curl_option($ch,CURLOPT_POSTFIELDS, $es_query);

			$result = curl_exec($ch);
			// in case of error
			if (curl_errno($ch)) {
				$file = __FILE__; // get the current file's path
				$line = __LINE__-2; // get the current file's line
				$log  = new LoggerController(); // create a new instance of the logger
				$log->Log('curl','alert',"Curl error ".curl_error($ch),$file,$line); // log to the log file
				curl_close($ch);
				return false;
			}
			curl_close($ch);
			$result = json_decode($result);
			if (!isset($result->error)) { // if it was stored
				return $result;
			}else{
				$file = __FILE__; // get the current file's path
				$line = __LINE__-2; // get the current file's line
				$log  = new LoggerController(); // create a new instance of the logger
				$log->Log('elasticsearch','error',$result->error,$file,$line); // log to the log file
				return false;
			}
		}else{
			$file = __FILE__; // get the current file's path
			$line = __LINE__-2; // get the current file's line
			$log  = new LoggerController(); // create a new instance of the logger
			$log->Log('elasticsearch','error',"The doc unable to search because the url was undefined",$file,$line); // log to the log file
			return false;
		}
	}

	public function delete_from_es($id){
		$ch       = self::get_curl_instance();
		$url = Config::get('alhuda.es_host')."/documents/doc/".$id;
		
		if (isset($url)) {
			self::set_curl_option($ch,CURLOPT_URL, $url);
			self::set_curl_option($ch,CURLOPT_PORT, 9200);
			self::set_curl_option($ch,CURLOPT_CUSTOMREQUEST, "DELETE");

			$result = curl_exec($ch);
			// in case of error
			if (curl_errno($ch)) {
				$file = __FILE__; // get the current file's path
				$line = __LINE__-2; // get the current file's line
				$log  = new LoggerController(); // create a new instance of the logger
				$log->Log('curl','alert',"Curl error ".curl_error($ch),$file,$line); // log to the log file
				return false;
			}
			$result = json_decode($result);
			if (isset($result->found)) { // if it was deleted
				return true;
			}else{
				$file = __FILE__; // get the current file's path
				$line = __LINE__-2; // get the current file's line
				$log  = new LoggerController(); // create a new instance of the logger
				$log->Log('elasticsearch','alert',"The doc was not deleted",$file,$line); // log to the log file
				return false;
			}
		}else{
			$file = __FILE__; // get the current file's path
			$line = __LINE__-2; // get the current file's line
			$log  = new LoggerController(); // create a new instance of the logger
			$log->Log('elasticsearch','alert',"The doc was not deleted, because there were no url defined",$file,$line); // log to the log file
			return false;
		}
	}

	public function es_bulk($es_query){
		$ch       = self::get_curl_instance();
		$url = Config::get('alhuda.es_host')."/documents/doc/_bulk";
		
		if (isset($url)) {
			self::set_curl_option($ch,CURLOPT_URL, $url);
			self::set_curl_option($ch,CURLOPT_PORT, 9200);
			self::set_curl_option($ch,CURLOPT_CUSTOMREQUEST, "POST");
			self::set_curl_option($ch,CURLOPT_POSTFIELDS, $es_query);

			$result = curl_exec($ch);
			// in case of error
			if (curl_errno($ch)) {
				$file = __FILE__; // get the current file's path
				$line = __LINE__-2; // get the current file's line
				$log  = new LoggerController(); // create a new instance of the logger
				$log->Log('curl','alert',"Curl error ".curl_error($ch),$file,$line); // log to the log file
				return false;
			}
			$result = json_decode($result);
			if (isset($result->errors) && $result->errors == true ) { // if it was deleted
				$file = __FILE__; // get the current file's path
				$line = __LINE__-2; // get the current file's line
				$log  = new LoggerController(); // create a new instance of the logger
				$log->Log('elasticsearch','alert',"Some errors were found with the bulk query",$file,$line); // log to the log file
				return false;
			}else{
				return true;
			}
		}else{
			$file = __FILE__; // get the current file's path
			$line = __LINE__-2; // get the current file's line
			$log  = new LoggerController(); // create a new instance of the logger
			$log->Log('elasticsearch','alert',"The doc was not deleted, because there were no url defined",$file,$line); // log to the log file
			return false;
		}
	}

	// count how many pages in totl do we have
	public function get_pages_number($total){
		$per_page = $this->per_page();
		if ($total%$per_page > 0) {
			$pages	= floor($total/$per_page)+1;
		}else{
			$pages	= $total/$per_page;
		}
		if ($pages>100) { // we don't want to show more than 100 pages ( it's exhausting to get 1000th page or so to elasticsearch )
			$pages = 100; 
		}
		return $pages;
	}

	// return how many results per page
	public function per_page()
	{
		return 10;
		// return 3;
	}
}