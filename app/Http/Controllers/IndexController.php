<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function search()
    {
    	$q = "";
    	if (isset($_GET['q'])) {
    		$q = urldecode($_GET['q']);
    	}
    	$p = 1;
    	if (isset($_GET['p'])) {
    		$p = $_GET['p'];
    	}

    	if (!empty($q)) {
	    	$sc = new SearchServerController();
	    	$result = $sc->search($q,$p);
	    	if ($result != "error") {
	    		$total = $sc->get_pages_number($result['total']);

                $data = array();

                foreach ($result['hits'] as $k => $v) {
                    $doc = $v['_source'];
                    $name = $doc['name'];
                    $cat = $doc['category'];
                    $author = $doc['author'];
                    $desc = $doc['description'];
                    $download_url = $doc['ahu_download_url'];
                    if (isset($v['highlight'])) {
                        $highlighted_version = $this->replace_with_highlight($v['highlight'],$author,$name,$desc);
                        $author = $highlighted_version['author'];
                        $name   = $highlighted_version['name'];
                        $desc   = $highlighted_version['desc'];
                    }

                    if (!empty($desc)) {
                        $desc = str_limit($desc,150); // limit the description string to 150
                    }

                    array_push($data, array("name"=>$name,"cat"=>$cat,"author"=>$author,"desc"=>$desc,"download_url"=>$download_url));
                }

                $total_results = $result['total'];
                
                $pagination = $this->build_pagination_html($total,$p);

	        	return view('results')->withData(array("data"=>$data,"total"=>$total_results))->withPagination($pagination);
	    	}else{
	        	return view('results')->withError("An error has occured, please try again later");
	    	}

    	}else{
	        return view('results')->withError("Empty Query");
    	}

    }

    public function ajax_search()
    {
        $q = "";
        if (isset($_POST['q'])) {
            // $q = urldecode($_GET['q']);
            $q = $_POST['q'];
        }
        $p = 1;
        if (isset($_POST['p'])) {
            $p = $_POST['p'];
        }

        $s = false;
        if (isset($_POST['s'])) {
            $s = $_POST['s'];
        }
        
        $sd = false;
        if (isset($_POST['sd'])) {
            $sd = $_POST['sd'];
        }

        $f = false;
        if (isset($_POST['f'])) {
            $f = $_POST['f'];
        }

        if (!empty($q)) {
            $sc = new SearchServerController();
            $total = 0;
            $result = $sc->search($q,$p,$s,$sd,$f);
            if ($result != "error"){
                $data = array();
                if (count($result)>0) {
                    if (isset($result['total'])) {
                        $total = $result['total'];
                    }
                    $hits = $result['hits'];
                    if (count($hits)>0) {
                        foreach ($hits as $k => $h) {
                            $d = $h['_source'];
                            if (isset($h['highlight'])) {
                                $highlighted_version = $this->replace_with_highlight($h['highlight'],$d['author'],$d['name'],$d['description']);
                                $d['author']         = $highlighted_version['author'];
                                $d['name']           = $highlighted_version['name'];
                                $d['description']    = $highlighted_version['desc'];
                            }
                            unset($d['document_url']);
                            unset($d['src_download_url']);

                            if (!empty($d['description'])) {
                                $d['description'] = str_limit($d['description'],150); // limit the description string to 150
                            }

                            $data[] = $d;
                        }
                    }
                }

                $pages = $sc->get_pages_number($result['total']);

                $pagination = $this->build_pagination_html($pages,$p);

                $resp = new \StdClass;
                $resp->pagination = $pagination;
                $resp->total = $total;
                $resp->data = $data;
                $resp = json_encode($resp);
                return response($resp, 200)->header('Content-Type', "application/json");
            }else{
                $resp = new \StdClass;
                $resp->error = true;
                $resp->message = "An error has occured, please try again later";
                $resp = json_encode($resp);
                return response($resp, 500)->header('Content-Type', "application/json");
            }
        }else{
            $resp = new \StdClass;
            $resp->error = true;
            $resp->message = "Empty Query";
            $resp = json_encode($resp);
            return response($resp, 500)->header('Content-Type', "application/json");
        }
    }


    // detect if the string is arabic ( by checking common letters )
    function is_arabic($str){
        if (strpos($str, "ا") !== false) {
            return true;
        }elseif (strpos($str, "م") !== false) {
            return true;
        }elseif (strpos($str, "ي") !== false) {
            return true;
        }elseif (strpos($str, "و") !== false) {
            return true;
        }elseif (strpos($str, "ن") !== false) {
            return true;
        }else{
            return false;
        }
    }

    // replace with highlighted version
    function replace_with_highlight($v_highlight,$author,$name,$desc){
        if (isset($v_highlight)) {
            $highlight = $v_highlight;
            if (isset($highlight['author'])) {
                $h = $highlight['author'][0];
                $hs = strip_tags($h);
                $author = explode($hs, $author);
                $author = implode($h, $author);
            }

            if (isset($highlight['name.ar']) || isset($highlight['name.en'])) {
                $ar = $this->is_arabic($name);
                if ($ar) {
                    if (isset($highlight['name.ar'][0])) {
                        $h = $highlight['name.ar'][0];
                    }elseif(isset($highlight['name.en'][0])){
                        $h = $highlight['name.en'][0];
                    }
                }else{
                    if (isset($highlight['name.en'][0])) {
                        $h = $highlight['name.en'][0];
                    }elseif(isset($highlight['name.ar'][0])){
                        $h = $highlight['name.ar'][0];
                    }
                }
                $hs = strip_tags($h);
                $name = explode($hs, $name);
                $name = implode($h, $name);
            }

            if (isset($highlight['description.ar']) || isset($highlight['description.en'])) {
                $ar = $this->is_arabic($desc);
                if ($ar) {
                    if (isset($highlight['description.ar'][0])) {
                        $h = $highlight['description.ar'][0];
                    }elseif ($highlight['description.en'][0]) {
                        $h = $highlight['description.en'][0];
                    }
                }else{
                    if (isset($highlight['description.en'][0])) {
                        $h = $highlight['description.en'][0];
                    }elseif ($highlight['description.ar'][0]) {
                        $h = $highlight['description.ar'][0];
                    }
                }
                $hs = strip_tags($h);
                $desc = explode($hs, $desc);
                $desc = implode($h, $desc);
            }
        }
        return array('author'=>$author,'name'=>$name,'desc'=>$desc);
    }

    // print the pagination links
    function do_paginate($pages,$curr_page,$url,$explode_on = "&")
    {
        if ($pages > 1) {
            $base_page_url = $url.$explode_on;
            $p = "";
            $limit = $pages+1;
            if ($pages>6) {
                for ($i=1; $i < 4; $i++) {
                    if ($curr_page == $i) {
                        $p .= '<li><a href="'.$base_page_url.'p='.$i.'" class="page active">'.$i.'</a></li>';
                        if ($i == 3) {
                            $j = $i+1;
                            $p .= '<li><a href="'.$base_page_url.'p='.$j.'" class="page">'.$j.'</a></li>';
                        }
                    }else{
                        $p .= '<li><a href="'.$base_page_url.'p='.$i.'" class="page">'.$i.'</a></li>';
                    }
                }

                if ($curr_page>3 && $curr_page<$pages-2) {
                    for ($i=4; $i < $pages-2; $i++) { 
                        if ($curr_page == $i) {
                            $h = $i-1;
                            if ($h>4) {
                                $p .= '<li>...</li>';
                            }
                            if ($h > 3) {
                                $p .= '<li><a href="'.$base_page_url.'p='.$h.'" class="page">'.$h.'</a></li>';
                            }
                            $p .= '<li><a href="'.$base_page_url.'p='.$i.'" class="page active">'.$i.'</a></li>';
                            $j = $i+1;
                            if ($j<$pages-2) {
                                $p .= '<li><a href="'.$base_page_url.'p='.$j.'" class="page">'.$j.'</a></li>';
                            }
                            if ($j<$pages-3) {
                                $p .= '<li>...</li>';
                            }
                        }
                    }   
                }else{
                    $p .= '<li>...</li>';
                }

                for ($i=$pages-2; $i < $limit; $i++) { 
                    if ($curr_page == $i) {
                        if ($i == $pages-2) {
                            $h = $i-1;
                            $p .= '<li><a href="'.$base_page_url.'p='.$h.'" class="page">'.$h.'</a></li>';
                            
                        }
                        $p .= '<li><a href="'.$base_page_url.'p='.$i.'" class="page active">'.$i.'</a></li>';
                    }else{
                        $p .= '<li><a href="'.$base_page_url.'p='.$i.'" class="page">'.$i.'</a></li>';
                    }
                }

            }else{
                for ($i=1; $i < $limit; $i++) { 
                    if ($curr_page == $i) {
                        $p .= '<li><a href="'.$base_page_url.'p='.$i.'" class="page active">'.$i.'</a></li>';
                    }else{
                        $p .= '<li><a href="'.$base_page_url.'p='.$i.'" class="page">'.$i.'</a></li>';
                    }
                }
            }
            return $p;
        }
    }

    // build the html of the pagination
    public function build_pagination_html($pages,$curr_page){
        if ($pages>0) {
            $request_url = $_SERVER['REQUEST_URI'];

            $url_arr = explode("&", $request_url);
            $pages_url = array();
            foreach ($url_arr as $v) {
                if (!strstr($v, "p=")) {
                    array_push($pages_url, $v);
                }
            }
            $pages_url = implode("&", $pages_url);
            $html = $this->do_paginate($pages,$curr_page,$pages_url);
            $html .= '<div class="clear"></div>';
            return $html;
        }else{
            return ""; 
        }
    }

}