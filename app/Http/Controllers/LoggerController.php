<?php namespace App\Http\Controllers;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\LineFormatter;

class LoggerController extends Controller {


	public function Log($channel, $logLevel, $msg, $file = null, $line = 0)
	{
		$defaultLine = __LINE__;
		switch ($logLevel) {
			case 'debug': // 100 detailed debug info
			case 'info': // 200 interesting events. i.e: user logs in
			case 'notice': // 250 normal but significant events
			case 'warning': // 300 exceptional occurences that are not errors, i.e undesirable things that are not necessarily wrong
			case 'error': // 400 runtime errors that do not require immediate action but should typically be logged and monitored
			case 'critical': // 500 critical conditions. i.e: app component unavailable 
			case 'alert': // 550 Action must be taken immediatly. i.e: entire website down, DB down
			case 'emergency': // 600 Emergency : system is unusable
				$dateFormat = "c"; //iso8601
				// the default output format is "[%datetime%] %channel%.%level_name%: %message% %context% %extra%\n"
				$output = "[%datetime%] %channel%:%level_name% %context% %message% \n";
				// finally, create a formatter
				$formatter = new LineFormatter($output,$dateFormat);
				$date = Date("Y-m-d");
				$ip = "127.0.0.1";
				if (isset($_SERVER["REMOTE_ADDR"])) {
					$ip = $_SERVER["REMOTE_ADDR"];
				}
				if ($channel == "laravel") {
					$handler = new StreamHandler(storage_path('logs'.DIRECTORY_SEPARATOR.'laravel-'.$date.'.log'));
				}else{
					$handler = new StreamHandler(storage_path('logs'.DIRECTORY_SEPARATOR.'ahu-logic-'.$date.'.log'));
				}
				$handler->setFormatter($formatter);
				$logger = new Logger($channel);
				$logger->pushHandler($handler,Logger::DEBUG);
				if ($channel == "laravel") {
					$logger->$logLevel($msg,array($ip));
				}else{
					$file = str_replace("\\", "/", $file);
					$logger->$logLevel($msg,array($file,$line,$ip));
				}
			break;
			default:
				$file = __FILE__;
				$line = $defaultLine-2;
				$func = "LOG()";
				error_log("PHP error: Loglevel \"".$logLevel."\" passed to function ".$func." was unexpected in ".$file." on line ".$line);
				break;
		}
	}

}