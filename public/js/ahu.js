$(function(){

	// set csrf header for each post request
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

	responsify();

	$(window).resize(function(){
		responsify();
	})

	function responsify() {
		var ww = $(window).width(); // window width
		if ( ww < 768 ) { // small screen
			if ($('#sidebar').hasClass('sidebar')) {
				$('.filter_div,.sort_div').show();
				$('#show_filter_div,#show_sort_div').show();
				$('#sidebar').addClass('alternative_sidebar col-xs-12').removeClass('sidebar col-xs-3');
				$('#results_content').addClass('col-xs-12').removeClass('col-xs-10 col-xs-offset-2').css('padding-left','15px');
				$('.filter_button,.sort_button').hide();
				$('#sidebar .form').hide();
				$('#filter_div_collapsed,#sort_div_collapsed').show();
				$('.close_section').remove();
				$('#loader').addClass('mobile');
			};
		}else{
			if ($('#sidebar').hasClass('alternative_sidebar')) {
				$('.sort_div').hide();
				$('#sidebar').removeClass('alternative_sidebar col-xs-12').addClass('sidebar col-xs-3');
				$('#results_content').addClass('col-xs-10 col-xs-offset-2').removeClass('col-xs-12').css('padding-left','100px');
				$('.filter_button,.sort_button').show();
				$('#sidebar .form').show();
				$('#filter_div_collapsed,#sort_div_collapsed').hide();
				$('.close_section').remove();
				$('#loader').removeClass('mobile');
			}else{
				if ($('.sort_button').hasClass('bg_sidebar_button')) {
					$('.filter_div').show();
					$('.sort_div').hide();
				}else{
					$('.filter_div').hide();
					$('.sort_div').show();
				}
			}
		}
	}

	$('#show_sort_div,#show_filter_div').on('click',function(e) {
		$('#sidebar').prepend('<button type="button" class="btn btn-default pull-right close_section"> <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> </button><div class="clearfix"></div>');
		$(this).parent().next().show();
		$('#filter_div_collapsed,#sort_div_collapsed').hide();
	});

	$('#sidebar').on('click','.close_section',function () {
		$('.close_section').remove();
		$('#sidebar .form').hide();
		$('#filter_div_collapsed,#sort_div_collapsed').show();
	})


	$('.filter_button,.sort_button').on('click',function (e) {
		$(this).removeClass('bg_sidebar_button');
		if ($(e.target).hasClass('filter_button')) {
			$('.sort_button').addClass('bg_sidebar_button');
			$('.filter_div').show();
			$('.sort_div').hide();
		}else{
			$('.filter_button').addClass('bg_sidebar_button');
			$('.filter_div').hide();
			$('.sort_div').show();
		}
	});
	
	// refresh results ( do a search )
	function refresh_results (d,id) {
		// loader
		$('#page_loader').show();
		label = false;
		if (id != false) {
			if (id == "sort_results") {
				label = "Sort";
			}else{
				label = "Filter";
			}
		};
		$.ajax({
			type: "POST",
			url: "results?q="+encodeURIComponent(d.q),
			data: d,
			success: function(d){
				$('#page_loader').hide();
				if (label) {
					$("#"+id).attr('disabled',false).addClass('btn-success').removeClass('btn-default').text(label);
				};
				var total = d.total;
				$('.document').remove();
				if (total>0) {
					if (total>1) {
						$('.documents_count').html('<b>'+total+'</b> documents were found.');
					}else{
						$('.documents_count').html('<b>'+total+'</b> document was found.');
					}

					var pages = d.pages;
					var data = d.data;
					var placeholder;
					var html = "";

					for (var i = 0; i < data.length; i++) {
						placeholder = data[i];
						desc = placeholder.description;

						html += '<div class="document"> <div class="doc_title"> <h3><a href="'+placeholder.ahu_download_url+'"><span class="download_icon glyphicon glyphicon-download-alt" aria-hidden="true"></span>'+placeholder.name+'</a></h3> </div> <span class="doc_desc">'+desc+'</span>';
						if (placeholder.author != "") {
							html +='<span class="doc_author"><span class="user_icon glyphicon glyphicon-user" aria-hidden="true"></span>'+placeholder.author+'</span>';
						}

						if (placeholder.category != "") {
							html += '<span class="doc_cat"><span class="tag_icon glyphicon glyphicon-tag" aria-hidden="true"></span>'+placeholder.category+'</span>';
						}
						html += '</div>';
					};						
					
					$('.documents_count').after(html);

					$('#pagination').html(d.pagination);

				}else{
					// no data 
					$('#pagination').html("");
					$('.documents_count').html('<h3 class="text-center"> No results were found ! </h3>');
				}
			},
			error: function(d){
				$('#page_loader').hide();
				if (label) {
					$("#"+id).attr('disabled',false).addClass('btn-success').removeClass('btn-default').text(label);
				}

				// pop a modal (error)
			}
		});
	}

	$('#sort_results,#filter_results').on('click',function (e) {
		var id = $(e.target).attr('id');
		var q = $('#search_field').val();
		// var p = $('#pagination .active').text();
		var p = 1; // override page to 1
		var s = $('#sortBy').val();
		var sd = $('#dir').val();
		var c = $('#category').val();
		var dt = $('#doc_title').val();
		var a = $('#author').val();
		var t = "";
		var d = "";
		var filter = {"c":c,"dt":dt,"a":a};

		$(this).attr('disabled',true).removeClass('btn-success').addClass('btn-default').text('Please Wait...');

		d = {"q":q,"p":p,"f":filter,"s":s,"sd":sd};

		refresh_results (d,id);
	});


	$('#results_content').on('click','.page',function (e) {
		e.preventDefault();
		var p = $.trim($(this).text());

		var q = $('#search_field').val();
		var s = $('#sortBy').val();
		var sd = $('#dir').val();
		var c = $('#category').val();
		var dt = $('#doc_title').val();
		var a = $('#author').val();
		var filter = {"c":c,"dt":dt,"a":a};

		// load the page
		d = {"q":q,"p":p,"f":filter,"s":s,"sd":sd};
		refresh_results (d,false);
	})

});