<form id="search_form" class="row search_container_elem center-block" action="results" method="GET"> <!-- Search Form -->
	<div class="input-group input-group-lg col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
		<?php
			if (isset($_GET['q'])) {$q = $_GET['q']; }else{$q = ""; }
		?>
		<input type="text" name="q" placeholder="What document are you looking for ?" id="search_field" class="form-control" aria-label="..." value="{{$q}}">
		<div class="input-group-btn">
			<button id="search" type="submit" class="btn btn-default btn-lg">
			  <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
			</button>
		</div>
	</div>
</form>