<?php
$total = $data['total']; // total documents
?>

@extends('app')
@section('title', "Search Results")

@section('content')
	<div id="ahu_results">
		<div id="results_header" class="row">
			<div id="header_logo" class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
				<a href="{{url('/')}}"><img src="img/logo.png" alt="ahu_logo"/></a>
			</div>
			@include('results_search_form')
		</div>
		<div class="row">
			@if ($total>0)
				<button type="button" title="Filter" class="btn btn-default filter_button"><span class="glyphicon glyphicon-filter"></span></button>
				<button type="button" title="Sort" class="btn btn-default sort_button bg_sidebar_button"><span class="glyphicon glyphicon-sort"></span></button>
				<div id="sidebar" class="col-xs-3 sidebar">
					<div class="filter_div">
						<div id="filter_div_collapsed"><button type="button" id="show_filter_div" class="btn btn-default center-block"><span class="glyphicon glyphicon-filter"></span> Filter</button></div>
						<div class="form">
							<div class="form-group">
								<label for="category">Category:</label>
								<select id="category" class="form-control">
									<option value="a">All Categories</option>
									<option value="1">Cat 1</option>
								</select>
							</div>
							<div class="form-group">
								<label for="doc_title">Title:</label>
								<input type="text" class="form-control" id="doc_title" placeholder="Document Title">
							</div>
							<div class="form-group">
								<label for="author">Author:</label>
								<input type="text" class="form-control" id="author" placeholder="Author Name">
							</div>

							<button type="button" id="filter_results" class="btn btn-success center-block">Filter</button>
						</div>

					</div>

					<div class="sort_div">
						<div id="sort_div_collapsed"><button type="button" id="show_sort_div" class="btn btn-default center-block"><span class="glyphicon glyphicon-sort"></span> Sort</button></div>
						<div class="form">
							<div class="form-group">
								<label for="sortBy">Sort By:</label>
								<select id="sortBy" class="form-control">
									<option value="re">Relevance</option>
									<option value="dt">Document Title</option>
									<option value="an">Author Name</option>
								</select>
							</div>
							<div class="form-group">
								<label for="dir">Sort Direction:</label>
								<select id="dir" class="form-control">
									<option value="desc">Descending</option>
									<option value="asc">Ascending</option>
								</select>
							</div>

							<button type="button" id="sort_results" class="btn btn-success center-block">Sort</button>
						</div>
					</div>
					
					<div class="clearfix"></div>

				</div>
			@endif
			<div id="results_content" class="col-xs-10 col-xs-offset-2">
				<div id="page_loader"><div id="loader"><img src="{{asset('img/loader.gif')}}" > Loading... </div></div>
				<div>
					@if (isset($error))
						<div class="error-container alert alert-danger alert-dismissible" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						  <strong>Error:</strong> {{$error}}.
						</div>
					@else
						@if (isset($data['data']))
							@if ($total>0)
								<span class="documents_count">
									@if ($total>1)
										<b>{{$total}}</b> documents were found.
									@else 
										<b>{{$total}}</b> document was found.
									@endif
								</span>
								
								@foreach ($data['data'] as $k => $v)
									<?php
										$name = $v['name'];
										$cat = $v['cat'];
										$author = $v['author'];
										$desc = $v['desc'];
										$download_url = $v['download_url'];

									?>
									<div class="document">
										
										<div class="doc_title">
											<h3><a href="{{$download_url}}"><span class="download_icon glyphicon glyphicon-download-alt" aria-hidden="true"></span> {!! $name !!}</a></h3>	
										</div>
										<span class="doc_desc">{!! $desc !!}</span>
										@if (!empty($author))
											<span class="doc_author"><span class="user_icon glyphicon glyphicon-user" aria-hidden="true"></span>{!! $author !!}</span>
										@endif
										@if (!empty($cat))
											<span class="doc_cat"><span class="tag_icon glyphicon glyphicon-tag" aria-hidden="true"></span>{{$cat}}</span>
										@endif
									</div>
								@endforeach
							@else
								<h3 class="text-center"> No results were found ! </h3>
							@endif
						@endif
					@endif

					@if (!empty($pagination))
						<ul id="pagination">
						<?php
							echo $pagination;
						?>
						</ul>
					@endif
				</div>
			</div>
		</div>
		<div id="results_footer">
			Footer
		</div>
	</div>	
@endsection