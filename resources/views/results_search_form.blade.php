<form id="results_search_form" action="results" method="GET" class="col-lg-8 col-md-8 col-sm-8 col-xs-12"> <!-- Search Form -->
	<div class="input-group input-group-lg">
		<?php
			if (isset($_GET['q'])) {$q = $_GET['q']; }else{$q = ""; }
		?>
		<input type="text" name="q" placeholder="What document are you looking for ?" id="search_field" class="form-control" aria-label="..." value="{{$q}}">
		<div class="input-group-btn">
			<button id="search" type="submit" class="btn btn-default btn-lg">
			  <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
			</button>

			<!--<button id="filter" type="button" class="btn btn-default btn-lg">
			  <span class="glyphicon glyphicon-filter" aria-hidden="true"></span>
			</button>-->
		</div>
	</div>
	<div id="search_filters">
	</div>
</form>