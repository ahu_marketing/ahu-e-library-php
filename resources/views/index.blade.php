@extends('app')
@section('title', "Home")

@section('content')
	<div id="ahu_index">
		<div id="index_content">
			<div id="logo" class="row center-block">
				<a href="{{url('/')}}"><img src="img/logo.png" alt="ahu_logo"/></a>
			</div>
			@include('search_form')
		</div>
	</div>	
@endsection